'use strict'

const {
  prependAlias,
  filterByTag,
  badSourceError,
  noCollectionError,
} = require('./json')

const { json } = require('.')

describe(`UNIT ${__dirname}`, () => {
  describe('prependAlias()', () => {
    describe('has alias', () => {
      test('returns alias-prefixed key', () => {
        expect(prependAlias('key', 'alias')).toEqual('alias.key')
      })
    })

    describe('no alias', () => {
      test('returns key', () => {
        expect(prependAlias('key')).toEqual('key')
      })
    })
  })

  describe('filterByTag()', () => {
    const tags = ['a', 'b', 'c']

    describe('tag does match', () => {
      test('returns true', () => {
        expect(filterByTag(tags, 'a')).toEqual(true)
      })
    })

    describe('tag does not match', () => {
      test('returns false', () => {
        expect(filterByTag(tags, 'x')).toEqual(false)
      })
    })

    describe('no tag to match', () => {
      test('returns true', () => {
        expect(filterByTag(tags)).toEqual(true)
      })
    })
  })
})

describe(`INTEGRATION ${__dirname}`, () => {
  describe('json().load()', () => {
    const collection = 'collection'

    const source = {
      collection: [
        {
          key: 'a',
          value: 'A'
        },
        {
          key: 'b',
          value: 'B',
          tags: [
            'tagged'
          ]
        },
        {
          key: 'c',
          value: 'C',
          tags: [
            'tagged',
            'untagged'
          ]
        },
        {
          key: 'd',
          value: 'D',
          tags: [
            'untagged'
          ]
        }
      ],
    }

    describe('collection string with no opts', () => {
      const output = [
        {
          key: 'a',
          value: 'A',
        },
        {
          key: 'b',
          value: 'B',
          tags: [
            'tagged'
          ],
        },
        {
          key: 'c',
          value: 'C',
          tags: [
            'tagged',
            'untagged'
          ],
        },
        {
          key: 'd',
          value: 'D',
          tags: [
            'untagged'
          ],
        }
      ]

      test('resolves loaded collection', () => {
        return expect(
          json(source).load(collection)
        ).resolves.toEqual(output)
      })
    })

    describe('select object with opts', () => {
      const alias = 'new'
      const tag = 'tagged'
      const output = [
        {
          key: 'new.b',
          value: 'B',
          tags: [
            'tagged'
          ],
        },
        {
          key: 'new.c',
          value: 'C',
          tags: [
            'tagged',
            'untagged'
          ],
        }
      ]

      test('resolves loaded collection', () => {
        return expect(
          json(source).load({ collection, alias, tag })
        ).resolves.toEqual(output)
      })
    })

    describe('no collection', () => {
      test('rejects no ', () => {
        return expect(
          json({}).load(collection)
        ).rejects.toEqual(noCollectionError(collection))
      })
    })

    describe('no source', () => {
      test('rejects no source error', () => {
        return expect(
          json().load(collection)
        ).rejects.toEqual(badSourceError())
      })
    })
  })
})
