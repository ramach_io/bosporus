'use strict'

const badSourceError = source => new Error(
  `Problem connecting with data source: ${source}`
)

const noCollectionError = collection => new Error(
  `Collection: '${collection}' not found.`
)

const prependAlias = (key, alias) => (
  alias ? `${alias}.${key}` : key
)

const filterByTag = (tags = [], match) => (
  match ? tags.includes(match) : true
)

const load = (
  badSourceError,
  noCollectionError,
  prependAlias,
  filterByTag
) => (
  source => select => (
    source && select ? (
      typeof select === 'string' ? (
        source[select] ? (
          Promise.resolve(source[select])
        ) : Promise.reject(noCollectionError(select))
      ) : (
        source[select.collection] ? (
          Promise.resolve(source[select.collection].reduce(
            (acc, { key, value, tags}) => (
              filterByTag(tags, select.tag) ? acc.concat([{
                key: prependAlias(key, select.alias),
                value,
                tags,
              }]) : acc
            ), []
          ))
        ) : Promise.reject(noCollectionError(select.collection))
      )
    ) : Promise.reject(badSourceError(source))
  )
)

const json = load => source => ({
  load: load(source)
})

module.exports = {
  badSourceError,
  noCollectionError,
  prependAlias,
  filterByTag,
  load,
  json,
}
