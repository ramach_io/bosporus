'use strict'

const {
  badSourceError,
  noCollectionError,
  prependAlias,
  filterByTag,
  load,
  json,
} = require('./json')

module.exports = {
  json: json(
    load(
      badSourceError,
      noCollectionError,
      prependAlias,
      filterByTag
    )
  ),
}
