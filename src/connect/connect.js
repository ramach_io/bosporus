'use strict'

const connectMore = buildStore => function connectMore(...sources) {
  const previous = this
  Promise.all(sources).then(sets => ({
    ...previous,
    _store: buildStore(sets, previous),
  }))
}

const undefinedOk = true

const connect = ({
  connectMore,
  getDefaultLocale,
  setLocale,
  buildStore,
  select,
}) => (...sources) => Promise.all(sources).then(sets => ({
  connect: connectMore(buildStore),
  _locale: getDefaultLocale(),
  locale: setLocale,
  _store: buildStore(sets),
  $: select(),
  $$: select(undefinedOk),
})).catch(err => { throw err })

module.exports = {
  connectMore,
  connect,
}
