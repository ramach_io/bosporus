'use strict'

const {
  duplicateKeyError,
  buildStore,
} = require('./store')

module.exports = {
  buildStore: buildStore(duplicateKeyError),
}
