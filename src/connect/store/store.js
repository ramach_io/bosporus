'use strict'

const duplicateKeyError = key => {
  throw new Error(
    `Key: '${key}' is already used.\nSuggestion: `
    + 'use the alias option in the driver load method and/or '
    + 'simply do not use duplicate keys in your collections.'
  )
}

const buildStore = duplicateKeyError => (
  (sets, previous = {}) => sets.reduce((outer, set) => ({
    ...outer,
    ...set.reduce((inner, { key, value }) => (
      outer[key] || inner[key] ? (
        duplicateKeyError(key)
      ) : {
        ...inner,
        [key]: value,
      }
    ), {})
  }), previous)
)

module.exports = {
  duplicateKeyError,
  buildStore,
}
