'use strict'

const {
  buildStore,
} = require('./store')

const duplicateKeyError = key => ({ error: key })
const buildS = buildStore(duplicateKeyError)

describe(`UNIT ${__dirname}`, () => {
  describe('buildStore()', () => {
    describe('no duplicate keys', () => {
      const setA = [
        { key: 'a', value: 'a' },
        { key: 'b', value: 'b' },
        { key: 'c', value: 'c' },
      ]

      const setB = [
        { key: 'd', value: 'd' },
        { key: 'e', value: 'e' },
        { key: 'f', value: 'f' },
      ]

      const input = [setA, setB]

      const output = {
        a: 'a',
        b: 'b',
        c: 'c',
        d: 'd',
        e: 'e',
        f: 'f',
      }

      test('returns flat store object', () => {
        expect(buildS(input)).toEqual(output)
      })
    })
    
    describe('duplicate key in set', () => {
      const setA = [
        { key: 'a', value: 'a' },
        { key: 'b', value: 'b' },
        { key: 'a', value: 'c' },
      ]

      const setB = [
        { key: 'd', value: 'd' },
        { key: 'e', value: 'e' },
        { key: 'f', value: 'f' },
      ]

      const input = [setA, setB]

      const output = {
        error: 'a',
        d: 'd',
        e: 'e',
        f: 'f',
      }

      test('returns duplicate key error', () => {
        expect(buildS(input)).toEqual(output)
      })
    })

    describe('duplicate key in another set', () => {
      const setA = [
        { key: 'a', value: 'a' },
        { key: 'b', value: 'b' },
        { key: 'c', value: 'c' },
      ]

      const setB = [
        { key: 'd', value: 'd' },
        { key: 'b', value: 'e' },
        { key: 'f', value: 'f' },
      ]

      const input = [setA, setB]

      const output = {
        a: 'a',
        b: 'b',
        c: 'c',
        error: 'b',
        f: 'f',
      }

      test('returns duplicate key error', () => {
        expect(buildS(input)).toEqual(output)
      })
    })

    describe('has previous store', () => {
      const previous = {
        a: 'a',
        b: 'b',
        c: 'c',
      }

      const setB = [
        { key: 'd', value: 'd' },
        { key: 'e', value: 'e' },
        { key: 'f', value: 'f' },
      ]

      const output = {
        a: 'a',
        b: 'b',
        c: 'c',
        d: 'd',
        e: 'e',
        f: 'f',
      }

      test('returns flat store object', () => {
        expect(buildS([setB], previous)).toEqual(output)
      })
    })
  })
})
