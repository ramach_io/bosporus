'use strict'

const notFoundError = (key, locale, undefinedOk) => {
  const message = `Constant: '${key}' `
    + `(locale: '${locale}') `
    + `does not exist.`

  if (undefinedOk)
    console.warn(message)
  else
    throw new Error(message)
}

const buildString = (template, args) => (
  template.reduce((acc, val) => (
    val.$ ? `${acc}${args[val.$]}` : `${acc}${val}`
  ), '')
)

const getConstant = (notFoundError, buildString) => (
  function getConstant({
    key,
    value,
    args,
    locale,
    type,
  }, undefinedOk) {
    return value === undefined ? (
      notFoundError(key, locale, undefinedOk)
    ) : (
      type !== 'json' && typeof value === 'object' ? (
        value.constructor === Array ? (
          buildString(value, args)
        ) : (
          getConstant({
            key,
            value: value[locale],
            args,
            locale,
          })
        )
      ) : value
    )
  }
)

const selectByKey = selector => selector.key || selector

const ifElse = (x, y) => x || y

const select = (getConstant, selectByKey) => undefinedOk => (
  function select(selector){
    const { _store, _locale } = this
    
    return getConstant({
      key: selectByKey(selector),
      value: _store[selectByKey(selector)],
      args: ifElse(selector.args, {}),
      locale: ifElse(selector.locale, _locale),
    }, undefinedOk)
  }
)

module.exports = {
  notFoundError,
  buildString,
  getConstant,
  selectByKey,
  ifElse,
  select,
}
