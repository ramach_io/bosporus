'use strict'

const {
  buildString,
  getConstant,
  selectByKey,
  ifElse,
} = require('./select')

const { select } = require('.')

describe(`UNIT ${__dirname}`, () => {
  describe('buildString()', () => {
    const template = ['hello', ' ', { $: 'place' }, '.']
    const args = { place: 'world' }
    const output = 'hello world.'

    test('builds string from template', () => {
      expect(buildString(template, args)).toEqual(output)
    })
  })


  describe('getConstant()', () => {
    const notFoundError = (key, locale) => (
      `not found error for ${key} in ${locale}`
    )
    const buildString = () => 'is template string'
    const getC = getConstant(notFoundError, buildString)
    const params = {
      key: 'key',
      args: {},
      locale: 'en-US',
    }

    describe('given a string value', () => {
      const input = {
        ...params,
        value: 'hello',
      }

      test('returns that string', () => {
        expect(getC(input)).toEqual('hello')
      })
    })

    describe('given a number value', () => {
      const input = {
        ...params,
        value: 42,
      }

      test('returns that number', () => {
        expect(getC(input)).toEqual(42)
      })
    })

    describe('given a boolean', () => {
      const input = {
        ...params,
        value: false,
      }

      test('returns that boolean', () => {
        expect(getC(input)).toEqual(false)
      })
    })

    describe('given an array value', () => {
      const input = {
        ...params,
        value: ['a', { $: 'b' }],
      }
      
      test('returns templated string', () => {
        expect(getC(input)).toEqual('is template string')
      })
    })

    describe('given an object value', () => {
      const input = {
        ...params,
        value: { 'en-US': 42 },
      }
      
      test('returns nested locale value', () => {
        expect(getC(input)).toEqual(42)
      })
    })

    describe('given a json type', () => {
      const input = {
        ...params,
        type: 'json',
        value: { 'en-US': 42 },
      }
      
      test('returns literal json object', () => {
        expect(getC(input)).toEqual({ 'en-US': 42 })
      })
    })

    describe('given an undefined value', () => {
      const errorMsg = 'not found error for key in en-US'

      test('returns not found error', () => {
        expect(getC(params)).toEqual(errorMsg)
      })
    })
  })


  describe('selectByKey()', () => {
    describe('selector is a string', () => {
      test('returns selector', () => {
        expect(selectByKey('hello')).toEqual('hello')
      })
    })

    describe('selector is an object', () => {
      const input = { key: 'hello' }

      test('returns selector key', () => {
        expect(selectByKey(input)).toEqual('hello')
      })
    })
  })


  describe('ifElse()', () => {
    describe('x exists', () => {
      const x = 'x'
      const y = 'y'

      test('returns x', () => {
        expect(ifElse(x, y)).toEqual('x')
      })
    })

    describe('x does not exist', () => {
      const x = undefined
      const y = 'y'

      test('returns y', () => {
        expect(ifElse(x, y)).toEqual('y')
      })
    })
  })
})

describe(`INTEGRATION ${__dirname}`, () => {
  describe('select()', () => {
    describe('critical path', () => {
      const C = {
        _store: {
          a: 1,
          b: 2,
          c: {
            'en-US': 3,
            'en-UK': 4,
          },
          d: {
            'en-US': [
              { $: 'place' },
              ' domination'
            ],
            'en-UK': [
              'hello ',
              { $: 'place' }
            ],
          },
        },
        _locale: 'en-UK',
        $: select()
      }

      test('C', () => {
        expect(C.$('a')).toEqual(1)
        expect(C.$('c')).toEqual(4)
        expect(C.$({
          key: 'd',
          args: { place: 'world' },
          locale: 'en-US',
        })).toEqual('world domination')
      })
    })
  })
})
