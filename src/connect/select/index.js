'use strict'

const {
  notFoundError,
  buildString,
  getConstant,
  selectByKey,
  select,
} = require('./select')

module.exports = {
  select: select(
    getConstant(
      notFoundError,
      buildString,
    ),
    selectByKey
  )
}
