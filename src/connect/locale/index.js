'use strict'

const {
  getDefaultLocale,
  setLocale,
} = require('./locale')

module.exports = {
  getDefaultLocale,
  setLocale,
}
