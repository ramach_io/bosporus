'use strict'

const {
  getDefaultLocale,
  setLocale,
} = require('./locale')

const defaultLocale = 'en-US'

describe(`UNIT ${__dirname}`, () => {
  describe('getDefaultLocale()', () => {
    test('return default locale', () => {
      expect(getDefaultLocale()).toBe(defaultLocale)
    })
  })

  describe('setLocale()', () => {
    describe('called with new locale', () => {
      const input = {
        locale: setLocale,
        _locale: defaultLocale,
      }

      const output = {
        locale: setLocale,
        _locale: 'en-UK',
      }

      test('returns new connection object with new locale', () => {
        expect(input.locale('en-UK')).toEqual(output)
      })
    })

    describe('called with no argument', () => {
      const input = {
        locale: setLocale,
        _locale: 'en-UK',
      }

      const output = {
        locale: setLocale,
        _locale: defaultLocale,
      }

      test('returns new connection object with default locale', () => {
        expect(input.locale()).toEqual(output)
      })
    })
  })
})
