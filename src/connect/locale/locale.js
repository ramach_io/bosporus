'use strict'

const getDefaultLocale = () => 'en-US'

function setLocale(locale) {
  return {
    ...this,
    _locale: locale || getDefaultLocale(),
  }
}

module.exports = {
  getDefaultLocale,
  setLocale,
}
