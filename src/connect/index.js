'use strict'

const { connect, connectMore } = require('./connect')
const { select } = require('./select')
const { buildStore } = require('./store')
const { getDefaultLocale, setLocale } = require('./locale')

module.exports = {
  connect: connect({
    connectMore,
    getDefaultLocale,
    setLocale,
    buildStore,
    select
  }),
}
